const express = require('express');
const router = express.Router();
const path = require('path');

var ctrlDir = path.resolve("controllers/");
var userController = require(path.join(ctrlDir, "User"));

//Middleware para mostrar datos del request
router.use(function (req, res, next) {
	console.log('/' + req.method);
	next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/', function (req, res) {
	res.sendFile(path.resolve('views/index.html'));
});

router.post('/edad', userController.add);
router.post('/edad', userController.edad);

module.exports = router;