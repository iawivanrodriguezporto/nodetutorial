
var User = require('../models/user');

//Create a new user and save it
var add = (req, res, next) => {
	var user = new User({ name: req.body.name, age: req.body.age });
	user.save();
	console.log(user);
	next();
	//return user;
};

//find all people
var list = (req, res, next) => {
	User.find(function (err, users) {
		return users;
	});
};

//find person by id
var find = (req, res) => {
	User.findOne({ _id: req.params.id }, function (error, user) {
		return user;
	})
};

var edad = (req, res) => {
	if (req.body.age >= 18) {
		res.send("Major d'edat");
	} else {
		res.send("Menor d'edat");
	}
}

module.exports = {
	add,
	list,
	find,
	edad
}